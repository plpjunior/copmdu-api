﻿using COPMDU.Infrastructure.ClassAttribute;
using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.PageModel
{
    public class CitOccurrence
    {

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>(\d+?)</small>", 
            ValueType = PageResultPropertyAttribute.ConversionType.Long)]
        public long Post { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>(.+?)</small>", 
            ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string Contract { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>(.+?)</small>",
            ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string Address { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>.+?</small>[\w\W]+?<small>\[ (\d+?) ]",
            ValueType = PageResultPropertyAttribute.ConversionType.Long)]
        public long Occurrence { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>.+?</small>[\w\W]+?<small>\[ \d+? ][\w\W]+?<small>(.+?)</small>",
            ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string Type { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>.+?</small>[\w\W]+?<small>\[ \d+? ][\w\W]+?<small>.+?</small>[\w\W]+?<small>(.+?)</small>",
            ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string Date { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>.+?</small>[\w\W]+?<small>\[ \d+? ][\w\W]+?<small>.+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small>(.+?)</small>",
            ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string Hour { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>.+?</small>[\w\W]+?<small>\[ \d+? ][\w\W]+?<small>.+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small>(.+?)</small>",
            ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Notification { get; set; }

        [PageResultProperty(@"<tr>\n[\w\W]+?<small>\d+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small><br>.+?</small>[\w\W]+?<small>\[ \d+? ][\w\W]+?<small>.+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small>.+?</small>[\w\W]+?<small>(.+?)</small>",
            ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string StatusNotification { get; set; }
    }
}
