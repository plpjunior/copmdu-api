﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Infrastructure.InterationException
{

    [Serializable]
    public class NoDataFoundException : Exception
    {
        public NoDataFoundException(string message) : base(message) { }
        protected NoDataFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
