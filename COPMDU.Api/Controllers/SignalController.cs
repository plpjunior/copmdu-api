﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using COPMDU.Domain;
using COPMDU.Domain.Entity;
using COPMDU.Infrastructure;
using COPMDU.Infrastructure.InterationException;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace COPMDU.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SignalController : ControllerBase
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly ISignalRepository _signalRepository;
        private readonly IConfiguration _config;
        private readonly ILogRepository _logRepository;

        private string _username => _config["Credentials:NewMonitor:User"];
        private string _password => _config["Credentials:NewMonitor:Password"];
        private int Timezone => int.Parse(_config["CopMdu:Timezone"]);

        public SignalController(ITicketRepository ticketRepository,
                                ISignalRepository signalRepository,
                                IConfiguration configuration,
                                ILogRepository logRepository)
        {
            _ticketRepository = ticketRepository;
            _signalRepository = signalRepository;
            _config = configuration;
            _logRepository = logRepository;
        }

        [AllowAnonymous]
        [HttpGet("CheckSignal/{ticketId}")]
        public async Task<Outage> CheckSignal(int ticketId)
        {
            try
            {
                var outage = _ticketRepository.Get(ticketId);

                if (outage == null)
                    throw new NoDataFoundException($"Não foi encontrado outage com o ID: {ticketId}");

                await _signalRepository.GetSignal(outage);

                outage.SignalExpiration = outage.SignalExpiration?.AddMinutes(Timezone);
                return outage;
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }
    }
}
