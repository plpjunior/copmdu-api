﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class Log
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public int Type { get; set; }
        public string Source { get; set; }
        public string StackTrace { get; set; }
        public string Message { get; set; }
        public int? Ticket { get; set; }
    }
}
